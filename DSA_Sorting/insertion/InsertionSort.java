package dsa.wrk.sorting.insertion;

public class InsertionSort {
	
	public static void printArray(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(" " + arr[i]);

		}

	}
	public static int[] inSort(int[] arr) {
		// int swap = 0;
		int l = 0;

		for (int i = 0; i < arr.length - 1; i++) {
			for (int j = i + 1; j > i; j--) {
				System.out.println(i + "(" + arr[i] + ")" + j + "{" + arr[j] + "}");
				int swap = arr[j];
				if (arr[i] > swap) {
					arr[j] = arr[i];
					arr[i] = swap;

					for (int j2 = j - 1; j2 >= 0; j2--) {

						if (arr[j2] > swap) {

							arr[j2 + 1] = arr[j2];
							arr[j2] = swap;

						}

					}

				}

			}

		}
		return arr;
	}
	public static int[] inSortMethod2(int[] arr) {

		int l = 0;

		for (int i = 1; i < arr.length - 1; i++) {
			int swap = arr[i];

			for (int j = i - 1; j > -1; j--) {
				arr[j + 1] = arr[j];
			}

			arr[i + 1] = swap;

		}
		return arr;
	}

}
